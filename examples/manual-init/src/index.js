/* eslint-disable import/first */
/* eslint-disable import/no-unresolved */
/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable max-classes-per-file */
/* eslint-disable class-methods-use-this */
import "./nastyDisgustingConsoleHack.js";

import Exorsa from "@exorsa/core";

class TestType {
	initialize(member) {
		console.log(member);
		return member.member;
	}
}

class TestMember {

}

class TestOtherMember {

}

const fx = new Exorsa({
	init: (...args) => console.log("Initialized", args),
	dependencies: [
		["TestType", "TestMember"]
	]
});

Promise.resolve()
	// Define Type
	.then(() => fx.define(TestType))
	// Log Result
	.then((test) => console.log({ test }))
	// Register Member
	.then(() => fx.register("TestType", {
		name: "TestMember",
		member: TestMember,
		dependencies: [
			["TestType", "TestOtherMember"]
		]
	}))
	// Log Result
	.then((...args) => console.log("TestMember promise", args));

setTimeout(() => {
	fx.register("TestType", {
		name: "TestOtherMember",
		member: TestOtherMember,
		dependencies: []
	});
}, 3500);

console.log({ fx });

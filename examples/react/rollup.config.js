import babel from '@rollup/plugin-babel';
import alias from '@rollup/plugin-alias';

const config = {
  input: 'src/index.jsx',
  output: {
    dir: 'lib',
    format: 'esm'
  },
  plugins: [
    babel({
      babelHelpers: 'bundled'
    }),
    alias({
      entries: [
        { find: '@exorsa/core', replacement: '../node_modules/@exorsa/core/lib/index.js' },
      ]
    })
  ]
};

export default config;


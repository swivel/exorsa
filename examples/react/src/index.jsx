/* eslint-disable import/first */
/* eslint-disable import/no-unresolved */
/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable max-classes-per-file */
/* eslint-disable class-methods-use-this */
import "./nastyDisgustingConsoleHack.js";

import Exorsa from "@exorsa/core";
import { React, ReactDOM } from "https://unpkg.com/es-react";

class Components {
	initialize(component) {
		console.log({ component });

		return component.member(component.dependencies);
	}
}

const HeadingComponent = () => ({ children }) => (
	<h1 id="heading">{children}</h1>
);

const RootComponent = ({
	components: { heading: Heading }
}) => ({ title }) => (
	<main>
		<Heading>{title}</Heading>
	</main>
);

const fx = new Exorsa({
	init: ({ components: { root: Root } }) => {
		console.log("Initialized!");

		ReactDOM.render(
			<Root title="boo" />,
			document.getElementById("root")
		);
	},
	dependencies: [
		["components", "root"]
	],
});

Promise.resolve()
	// Define Type
	.then(() => fx.define(Components))
	// Log Result
	.then((test) => console.log({ test }))
	.then(() => fx.register("components", {
		name: "root",
		member: RootComponent,
		dependencies: [
			["components", "heading"]
		],
	})
	)
	// Log Result
	.then((...args) => console.log("Reducer/Counter promise", args));

setTimeout(() => {
	fx.register("components", {
		name: "heading",
		member: HeadingComponent
	})
		// Log Result
		.then((...args) => console.log("Heading Component promise", args));
}, 3500);

console.log({ fx });

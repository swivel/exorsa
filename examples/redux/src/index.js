/* eslint-disable import/first */
/* eslint-disable import/no-unresolved */
/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable max-classes-per-file */
/* eslint-disable class-methods-use-this */
import "./nastyDisgustingConsoleHack.js";

import Exorsa from "@exorsa/core";
import { createStore } from "redux";

class Reducers {
	initialize(member) {
		console.log(member);
		const store = createStore(member.member);
		const reducerInterface = {
			dispatch: (...args) => store.dispatch(...args),
			getState: (...args) => store.getState(...args),
			subscribe: (...args) => store.subscribe(...args),
		};

		return reducerInterface;
	}
}

const counterReducer = (state = { value: 0 }, action) => {
	switch (action.type) {
		case "counter/decrement":
			return { value: state.value - 1 };
		case "counter/increment":
			return { value: state.value + 1 };
		default:
			return state;
	}
};

const fx = new Exorsa({
	init: ({ reducers: { counter } }) => {
		console.log("Initialized!");

		counter.subscribe(() => {
			console.log("Counter State:", counter.getState());
		});

		counter.dispatch({ type: "counter/increment" });
		counter.dispatch({ type: "counter/increment" });
		counter.dispatch({ type: "counter/increment" });

		counter.dispatch({ type: "counter/decrement" });
	},
	dependencies: [
		["reducers", "counter"]
	],
});

Promise.resolve()
	// Define Type
	.then(() => fx.define(Reducers))
	// Log Result
	.then((test) => console.log({ test }));

setTimeout(() => {
	fx.register("reducers", {
		name: "counter",
		member: counterReducer
	})
		// Log Result
		.then((...args) => console.log("Reducer/Counter promise", args));
}, 3500);

console.log({ fx });

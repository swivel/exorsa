const oldConsole = {
	log: console.log,
	warn: console.warn,
	error: console.error,
	info: console.info
};

const consoleWrap = (fn) => (...args) => {
	let e;
	try { throw new Error(""); } catch (err) { e = err; }
	const stack = `${e.stack}`.split(/\r\n|\n/);
	return fn(...args, `${stack[2]}`.trim());
};

Object.defineProperty(console, "log", { value: consoleWrap(oldConsole.log) });
Object.defineProperty(console, "warn", { value: consoleWrap(oldConsole.warn) });
Object.defineProperty(console, "error", { value: consoleWrap(oldConsole.error) });
Object.defineProperty(console, "info", { value: consoleWrap(oldConsole.info) });

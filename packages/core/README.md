# Exorsa

Exorsa provides registries and dependency-aware initialization for accessing
and managing your framework's types and their members. By aggregating types and
members, Exorsa provides a dynamic module system for automating framework APIs
and module initialization.

Future-proof your framework, with compatibility with present-day bundlers in addition to HTTP/3.0.


## How Does It Work?

Exorsa manages two types of items: **`Types`**, and **`Members`**.

**`Types`** are classes that help construct their **`Members`** (through **`Initializers`**). They also manage their access to other types and members of the framework through **`Interfaces`**.

**`Members`** are instances of a particular **`Type`**. These may be created by the framework/library developers, or an end-user developer of a framework. Their access to the framework is augmented by the **`Interface`** provided by their parent **`Type`**.

A `member` can specify one or more dependencies. Until these dependencies are initialized themselves, the `member` will wait to initialize. With bundler and HTTP/2.0 support, this allows dependencies to be loaded into the application only when a particular member requests them.


## Features

 - **Dependency Injection and Management**: Specify dependencies for each Member instance, ensuring that everything happens in the correct order.
 - **Custom Initialization**: Use `Types` to create custom functions that will be used to initialize each `Member`.
 - **Custom API**: Create a unique Interface to your framework for each type of member.


## Roadmap Items

 - `IN PROGRESS` **Automated Initialization**
 - `IN PROGRESS` **Automated Interfaces**
 - `PLANNED` **Bundling and Delivery**
 	- `PLANNED` **HTTP/3.0 support**
 	- `PLANNED` **Webpack Support**
 	- `PLANNED` **Rollup Support**
 	- `PLANNED` **Parcel Support**

/* eslint-disable import/prefer-default-export */

/* c8 ignore start */
const ERROR_NOT_MY_FAULT = "This most likely isn't an issue with exorsa, but with the library or framework you're using. Please submit an issue ticket to the owners of this package.";

const ERRORS = {
	Exorsa: {
		use: {
			requiredMiddleware: `exorsa::agent.use( middleware ): Invalid required parameter: middleware must be a function or class. ${ERROR_NOT_MY_FAULT}`,
		},
		retrieve: {
			rejectedType: (typeName) => (
				`exorsa::agent.retrieve( type: ${typeName} ): An error occurred when trying to retrieve the referenced type. ${ERROR_NOT_MY_FAULT}`
			),
			rejectedTypeMember: (typeName, memberName) => (
				`exorsa::agent.retrieve( type: "${typeName}", member: "${memberName}" ): An error occurred when trying to retrieve the referenced type member. ${ERROR_NOT_MY_FAULT}`
			),
		}
	},
	ExorsaType: {
		initialize: (name) => `exorsa::Type[${name}].initialize(): Class Method Not Implemented. ${ERROR_NOT_MY_FAULT}`
	},
	TypeRegistry: {
		describe: {
			requiredType: (type) => (
				`exorsa::TypeRegistry.describe( type: "${type}" ): Missing required parameter: type. ${ERROR_NOT_MY_FAULT}`
			),
		},
		get: {
			requiredType: (type, member) => (
				`exorsa::TypeRegistry.get( type: ${type}[, member${member ? `: ${member}` : ""} ] ): type must be a populated string, not ${typeof type}. ${ERROR_NOT_MY_FAULT}`
			),
			requiredMember: (type, member) => (
				`exorsa::TypeRegistry.get( type: ${type}[, member: ${member} ] ): member must be a populated string or undefined, not ${typeof member}. ${ERROR_NOT_MY_FAULT}`
			),
		},
		define: {
			typeIsNotClass: (name, TypeClass) => (
				`exorsa::TypeRegistry.define( [name${name === TypeClass ? "" : `: ${name}`},] TypeClass: ${TypeClass} ): TypeClass must be a Class, usually extends exorsa::Type. ${ERROR_NOT_MY_FAULT}`
			),
			typeAlreadyDefined: (typeName) => (
				`exorsa::TypeRegistry.define( TypeClass: ~'${typeName}' ): Type is already defined. Types should only be defined once. ${ERROR_NOT_MY_FAULT}`
			),
		},
	},
	MemberRegistry: {
		get: {
			requiredMember: (type) => (
				`exorsa::TypeRegistry[${type}].MemberRegistry.get( member ): Missing required parameter: member. ${ERROR_NOT_MY_FAULT}`
			),
		},
		register: {
			requiredMemberDesc: (type) => (
				`agent::TypeRegitry[${type}].MemberRegistry.register( memberDescriptor ): Invalid required parameter: memberDescriptor must be an object with 'name' and 'member' properties. ${ERROR_NOT_MY_FAULT}`
			),
			memberAlreadyDefined: (type, memberName) => (
				`agent::TypeRegistry[${type}].MemberRegistry.register( memberDescriptor: ~'${memberName}' ): Member is already registered. Members should only be registered once. ${ERROR_NOT_MY_FAULT}`
			),
		}
	},
	utils: {
		deepGet: {
			getFromNonObject: (path, key, type) => (
				`exorsa::utils: Object[${path}]: Cannot get key ${key} from non-object of type ${type}. ${ERROR_NOT_MY_FAULT}`
			),
		},
	},
};

export { ERRORS };
/* c8 ignore stop */

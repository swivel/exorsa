import TypeRegistry from "./registry/type.js";
import Type from "./type.js";

import { ERRORS } from "./copy.js";
import { deepArrayToObject } from "./utils.js";

/**
 * Private: WeakMap for storing instance properties that shouldn't
 * be accessed publicly. Automatically garbage collected when instance
 * is deconstructed and references are purged.
 *
 * @type {WeakMap}
 */
const Private = new WeakMap();

/**
 * Middleware: WeakMap for storing plugin instances that shouldn't
 * be accessed publicly. Automatically garbage collected when instance
 * is deconstructed and references are purged.
 */
const Middleware = new WeakMap();

export { Type };

export default class Exorsa {
	/* eslint-disable class-methods-use-this */
	get Type() { return Type; }

	set Type(v) { return Type; }
	/* eslint-enable */

	/**
	 * @method constructor
	 * @param  {function} init Executed after all dependencies are met
	 * @param  {array} dependencies Array of dependencies to load before init()
	 */
	constructor({
		init,
		dependencies = [],
		middleware = [],
		agent = this,
		bootstrapAgent = false,
	}) {
		if (bootstrapAgent) {
			const exorsaProto = Exorsa.prototype;
			const exorsaNames = Object.getOwnPropertyNames(exorsaProto);
			const agentProto = Object.getPrototypeOf(agent);
			const agentNames = Object.getOwnPropertyNames(agentProto);

			exorsaNames.forEach((m) => {
				if (m === "constructor") return;
				if (typeof exorsaProto[m] !== "function") return;
				if (agentNames.indexOf(m) >= 0) return;
				/* eslint-disable no-param-reassign */
				agent[m] = exorsaProto[m].bind(this);
			});
		}

		Private.set(this, {
			/**
			 * The primary interface that is passed around for interacting with the framework
			 * @type {TypeRegistry}
			 */
			agent,

			/**
			 * Maintains a list of all the currently defined Types
			 * @type {TypeRegistry}
			 */
			registry: new TypeRegistry(this, agent)
		});

		Middleware.set(this, new Set(Array.from(middleware)));

		this.getAll(dependencies).then(init);
	}

	/**
	 * Adds a middleware onto the stack
	 *
	 * @method use
	 * @param  {function} fn Plugin to adopt
	 * @throws TypeError If fn is not a class or function
	 */
	use(mw) {
		// const { agent } = Private.get(this);

		if (typeof mw !== "function") {
			throw new TypeError(ERRORS.Exorsa.requiredMiddleware);
		}

		Middleware.get(this).add(mw);
	}

	/**
	 * Shorthand for getType and getMember methods
	 *
	 * @method get
	 * @param  {String} type Identifier for requested Type
	 * @param  {String} [member] Identifier for Type's Member
	 * @return {!Promise} Promise from getType or getMember
	 */
	get(type, memberName) {
		const { registry } = Private.get(this);
		return registry.get(type, memberName);
	}

	/**
	 * Announces to all Middleware functions that a member is being requested
	 *
	 * @method retrieve
	 * @param  {String} type Identifier for requested Type
	 * @param  {String} [member] Identifier for Type's Member
	 * @return void
	 */
	retrieve(type, memberName) {
		// Grab all registered middlewares
		const middlewares = Middleware.get(this);
		const { agent } = Private.get(this);

		// Iterate through each and pass alone the type and memberName
		Array.from(middlewares).forEach((fn) => (
			fn(type, memberName, agent)
		));
	}

	/**
	 * Promises all members in list array
	 *
	 * @method getAll
	 * @param  {Array} list Array of [type, member] arrays
	 * @return {!Promise} Promises from this.get()
	 */
	getAll(list) {
		// If the list is empty, resolve with nothing
		if (!list || !list.length) return Promise.resolve([]);

		const { registry } = Private.get(this);

		try {
			// Promise all of the get calls
			return Promise.all(
				list.map(
					(refs) => registry.get(...[].concat(refs))
				)
			).then((results) => deepArrayToObject(list.map(
				([t, m]) => [m ? t : "types", m ?? t]
			), results));
		} catch (e) {
			console.error("exorsa::getAll( list ): An error occurred:", e);
			return Promise.reject(e);
		}
	}

	define(...args) {
		const { registry } = Private.get(this);
		return registry.define(...args);
	}

	register(type, member) {
		const { registry: typeRegistry } = Private.get(this);
		const { registry: memberRegistry } = typeRegistry.describe(type);
		return memberRegistry.register(member);
	}
}

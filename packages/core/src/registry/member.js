import { ERRORS } from "../copy.js";
import { formatName, initializeIfNeeded } from "../utils.js";

/**
 * Private: WeakMap for storing instance properties that shouldn't
 * be accessed publicly. Automatically garbage collected when instance
 * is deconstructed and references are purged.
 *
 * @type {WeakMap}
 */
const Private = new WeakMap();

/**
 * MemberRegistry
 */
export default class MemberRegistry {
	constructor({
		type,
		promise: parent
	}, agent) {
		Private.set(this, {
			agent,
			type,
			parent,
			registry: new Set(),
			resolvers: new Map(),
			promises: new Map()
		});
	}

	/**
	 * Promises to retrieve the Member of a Type
	 *
	 * @method getMember
	 * @param  {String} type Identifier of Member's Type
	 * @param  {String} member Identifier of the Member
	 * @return {!Promise} Promise, fulfilled when the Member is registered
	 */
	get(member) {
		// Grab all our private instance properties
		const {
			agent,
			type,
			resolvers,
			promises,
			parent
		} = Private.get(this);

		if (typeof member !== "string" || member.length === 0) {
			return Promise.reject(
				new TypeError(
					ERRORS.MemberRegistry.get.requiredMember(type)
				)
			);
		}

		// Normalize the member identifier
		const memberName = formatName(member);

		// Is there already a Promise for the Member we're requesting?
		// If so, return it.
		// Otherwise, create it, ensuring that it only resolves after its Type.
		return initializeIfNeeded(promises, memberName, () => (
			new Promise((resolve, reject) => {
				// Save the resolve and reject methods
				// so that we can invoke them later
				resolvers.set(memberName, {
					resolve: (value) => parent.then(() => resolve(value)),
					reject
				});

				// Ask Exorsa to retrieve the member
				Promise.resolve(
					agent.retrieve(type, memberName)
				).catch(reject);
			})
		).catch((error) => {
			console.error(
				ERRORS.retrieve.rejectedTypeMember(type, memberName)
			);
			throw error;
		}));
	}

	/**
	 * Registers a Type's Member
	 *
	 * @method register
	 * @param  {String} type Identifier for Type
	 * @param  {TypeMember} memberDescriptor Descriptor for Member
	 * @return {!Promise} Promise, fulfilled when Member is registered
	 */
	register(memberDescriptor) {
		// Make sure no one has already registered this member
		const {
			agent,
			type,
			parent,
			registry,
			resolvers
		} = Private.get(this);

		if (
			!memberDescriptor
			|| !("name" in memberDescriptor)
			|| !("member" in memberDescriptor)
		) {
			return Promise.reject(
				new TypeError(
					ERRORS.MemberRegistry.register.requiredMemberDesc(type)
				)
			);
		}

		// Clean up type and member names
		const memberName = formatName(memberDescriptor.name);

		// Get this member's dependencies and its member
		const { dependencies = {}, member } = memberDescriptor;

		if (registry.has(memberName)) {
			return Promise.reject(
				new RangeError(
					ERRORS.MemberRegistry.register.memberAlreadyDefined(
						type, memberName
					)
				)
			);
		}
		// Let everyone know that we're registering this member
		registry.add(memberName);

		// Grab the promise for this Type's Member
		const promise = this.get(memberName);

		// Wait until the Type is defined and all of our dependencies are done
		// being registered themselves before proceeding
		Promise.all([
			parent,
			agent.getAll(dependencies)
		]).then(([typeInstance, deps]) => (
			// Finally, pass our member to its Type's initialze method
			typeInstance.initialize({
				name: memberName,
				type,
				member,
				dependencies: deps,
				originalDescriptor: memberDescriptor
			})
		)).then(
			// Finally, we're officially registered
			(initialized) => resolvers.get(memberName).resolve(initialized),
			// Grab the resolve and reject for this Member
			(error) => resolvers.get(memberName).reject(error)
		);

		// Return promise, fulfilled when registered, rejected on failure
		return promise;
	}
}

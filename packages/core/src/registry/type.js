import MemberRegistry from "./member.js";
import { ERRORS } from "../copy.js";
import { isClass, formatName, initializeIfNeeded } from "../utils.js";

/**
 * Private: WeakMap for storing instance properties that shouldn't
 * be accessed publicly. Automatically garbage collected when instance
 * is deconstructed and references are purged.
 *
 * @type {WeakMap}
 */
const Private = new WeakMap();

/**
 * TypeRegistry
 */
export default class TypeRegistry {
	/**
	 * @method constructor
	 * @param  {Exorsa} exorsa Current instance of Exorsa
	 */
	constructor(exorsa, agent) {
		Private.set(this, {
			exorsa,
			agent,
			registry: new Set(),
			members: new Map(),
			resolvers: new Map(),
			promises: new Map()
		});
	}

	/**
	 * Describe the specified type, returning its name, promise, and registry.
	 *
	 * @method describe
	 * @param  {string} type The name of the type to describe
	 * @return {object} Plain object containing name, promise, and registry of type
	 */
	describe(type) {
		if (typeof type !== "string") {
			throw new TypeError(
				ERRORS.TypeRegistry.describe.requiredType(type)
			);
		}

		// Gets a clean version of type's name (e.g. 'FooBaR' -> 'foobar')
		const typeName = formatName(type);

		// Grab all our private instance properties
		const {
			agent,
			resolvers,
			promises,
			members
		} = Private.get(this);

		// Is there a Promise for the type we're asking for?
		// If yes, return it.
		// If not, set it up so that it can be resolved whenever we
		// finally get the Type the developer is asking for.
		const promise = initializeIfNeeded(promises, typeName, () => (
			new Promise((resolve, reject) => {
				// Save the resolve and reject methods
				// so that we can invoke them later
				resolvers.set(typeName, { resolve, reject });

				// Ask Exorsa to retrieve the type
				Promise.resolve(
					agent.retrieve(typeName)
				).catch(reject);
			})
		).catch((error) => {
			console.error(
				ERRORS.retrieve.rejectedType(typeName)
			);
			throw error;
		}));

		// Is there a members[typeName]? If not, set it to an empty Map
		const memberRegistry = initializeIfNeeded(members, typeName, () => (
			new MemberRegistry({
				type: typeName,
				promise
			}, agent)
		));

		return {
			type: typeName,
			promise,
			registry: memberRegistry
		};
	}

	/**
	 * Promises the specified type, or one of its members if also specified.
	 *
	 * @method get
	 * @param  {string} type The name of the type to promise
	 * @param  {string} member The name of the member to promise (optional)
	 * @return {!Promise} Promise, fulfills after Type or Member is ready
	 */
	get(type, member) {
		if (typeof type !== "string" || type.length === 0) {
			return Promise.reject(
				new TypeError(
					ERRORS.TypeRegistry.get.requiredType(type, member)
				)
			);
		}

		if (typeof member !== "undefined" && (typeof member !== "string" || member.length === 0)) {
			return Promise.reject(
				new TypeError(
					ERRORS.TypeRegistry.get.requiredMember(type, member)
				)
			);
		}

		const { promise, registry } = this.describe(type);

		if (typeof member !== "undefined") return registry.get(formatName(member));

		return promise;
	}

	/**
	 * Defines and Constructs a Type based on a Class Object
	 *
	 * @method define
	 * @param  {Type-like} TypeClass Type to be initialized
	 * @return {!Promise} Promise, fulfills after Type is constructed
	 */
	define(name, TypeClass = name) {
		// TypeClass must be a prototype object
		if (!isClass(TypeClass)) {
			return Promise.reject(
				new TypeError(
					ERRORS.TypeRegistry.define.typeIsNotClass(name, TypeClass)
				)
			);
		}

		// Clean up the type's name (e.g., 'FooBaR' => 'foobar')
		const typeName = formatName(name === TypeClass ? TypeClass.name : name);

		// Grab our private instance properties
		const {
			agent,
			registry,
			resolvers,
			// members
		} = Private.get(this);

		// Has someone already defined this type?
		// If so, reject the Promise with an error
		if (registry.has(typeName)) {
			return Promise.reject(
				new RangeError(
					ERRORS.TypeRegistry.define.typeAlreadyDefined(typeName)
				)
			);
		}
		// Let the next person know we've defined this type so they don't
		registry.add(typeName);

		// Get the latest promise for this Type
		const {
			promise,
			registry: memberRegistry
		} = this.describe(typeName);

		// Grab the resolve and reject for this Type's pending promise
		const { resolve, reject } = resolvers.get(typeName);

		try {
			// Resolve the promise with the TypeClass's instance
			resolve(new TypeClass({
				type: typeName,
				registry: memberRegistry
			}, agent));
		} catch (e) {
			// Reject the promise if construction fails
			reject(e);
		}

		// Return the promise
		return promise;
	}
}

/* eslint-disable import/first, no-unused-vars, max-classes-per-file */

import util from "util";
import assert from "assert";
import sinon from "sinon";
import crypto from "crypto";
import { ERRORS } from "../copy.js";
import { WeakMapSpy, randomString } from "../../../../test-utils.js";

import TypeRegistry from "./type.js";
import MemberRegistry from "./member.js";

let spyWeakMap;
let fakeType;
let mockExorsa;
let mockAgent;
let mockMemberDescriptor;
let Instance;
/* eslint-disable no-unused-vars */
let mockParent;
/* eslint-enable no-unused-vars */

describe("@exorsa/core::TypeRegistry", () => {
	before(() => {
		spyWeakMap = new WeakMapSpy();
		fakeType = sinon.fake();
		mockExorsa = {
			get: sinon.fake.returns(
				Promise.resolve(fakeType)
			),
			getAll: sinon.fake.returns(
				Promise.resolve([fakeType])
			),
		};

		mockAgent = {
			retrieve: sinon.fake.returns(() => Promise.resolve()),
		};

		mockMemberDescriptor = class {
			static name = "typename";

			static type = fakeType;
		};

		/* eslint-disable no-unused-vars */
		mockParent = {
			initialize: sinon.fake.returns(fakeType)
		};
		/* eslint-enable no-unused-vars */
	});

	beforeEach(() => {
		Instance = new TypeRegistry(mockExorsa, mockAgent);
	});

	afterEach(() => {
		spyWeakMap.delete(Instance);
		sinon.reset();
	});

	describe("constructor()", () => {
		it("should store arguments in a private weakmap", () => {
			const Private = spyWeakMap.get(Instance);

			assert.strictEqual(Private.exorsa, mockExorsa);
			assert.ok(
				Private.registry instanceof Set,
				"registry should be a Set"
			);
			assert.ok(
				Private.members instanceof Map,
				"members should be a Map"
			);
			assert.ok(
				Private.resolvers instanceof Map,
				"resolvers should be a Map"
			);
			assert.ok(
				Private.promises instanceof Map,
				"promises should be a Map"
			);
		});
	});

	describe("describe()", () => {
		it("should reject if no type name is passed", () => {
			const expected = ERRORS.TypeRegistry.describe.requiredType(
				undefined
			);
			assert.throws(
				Instance.describe.bind(Instance), (err) => {
					assert.equal(err.message, expected);
					return true;
				}, "should throw ERRORS.TypeRegistry.describe.requiredType"
			);
		});

		it("should reject if an invalid type name is passed", () => {
			const expected = ERRORS.TypeRegistry.describe.requiredType(`${{}}`);
			assert.throws(
				() => Instance.describe({}), (err) => {
					assert.equal(err.message, expected);
					return true;
				}, "should throw ERRORS.TypeRegistry.describe.requiredType"
			);
		});

		describe("agent.retrieve()", () => {
			it("should request type from exorsa if the type does not yet exist", () => {
				const fakeTypeName = randomString();
				Instance.describe(fakeTypeName);

				assert.ok(
					mockAgent.retrieve.lastCall.calledWith(fakeTypeName),
					"retrieve() must be called with typeName requested"
				);
			});

			it("should reject type if exorsa agent.retrieve() is rejected", () => {
				const expectedError = randomString();
				const mockLocalAgent = {
					retrieve: sinon.fake.returns(
						() => Promise.reject(expectedError)
					),
				};

				const localInstance = new TypeRegistry(
					mockExorsa, mockLocalAgent
				);
				const Private = spyWeakMap.get(localInstance);

				const fakeTypeName = randomString();
				localInstance.describe(fakeTypeName);

				assert.ok(
					mockLocalAgent.retrieve.lastCall.calledWith(fakeTypeName),
					"retrieve() must be called with typeName requested"
				);
				assert.rejects(
					Private.promises.get(fakeTypeName), expectedError
				);
			});
		});

		it("should return a new promise if the type does not yet exist", () => {
			const Private = spyWeakMap.get(Instance);

			const fakeTypeName = randomString();

			assert.ok(
				!Private.registry.has(fakeTypeName),
				"type should not exist in registry yet"
			);
			assert.ok(
				!Private.resolvers.has(fakeTypeName),
				"type should not have a resolver yet"
			);
			assert.ok(
				!Private.promises.has(fakeTypeName),
				"type should not have a promise yet"
			);

			const actual = Instance.describe(fakeTypeName);
			const expected = {
				type: fakeTypeName,
				promise: Private.promises.get(fakeTypeName),
				registry: Private.members.get(fakeTypeName)
			};

			// Registry signifies when a type has been registered
			assert.ok(
				!Private.registry.has(fakeTypeName),
				"type still shouldn't exist in registry yet"
			);

			// Resolvers and promises should be populated regardless
			assert.ok(
				Private.resolvers.has(fakeTypeName),
				"type should have a resolver now"
			);
			assert.ok(
				Private.promises.has(fakeTypeName),
				"type should have a promise now"
			);
			assert.deepStrictEqual(actual, expected);
		});

		it("should return the same promise for every request to the same type", () => {
			const Private = spyWeakMap.get(Instance);

			const fakeTypeName = randomString();

			const actual1 = Instance.describe(fakeTypeName);
			const actual2 = Instance.describe(fakeTypeName);
			const expected = {
				type: fakeTypeName,
				promise: Private.promises.get(fakeTypeName),
				registry: Private.members.get(fakeTypeName)
			};

			assert.deepStrictEqual(actual1, expected);
			assert.deepStrictEqual(actual2, expected);
		});
	});

	describe("get()", () => {
		it("should reject if no type name is passed", () => {
			const expected = ERRORS.TypeRegistry.get.requiredType();
			assert.rejects(
				Instance.get.bind(Instance), expected,
				"should throw ERRORS.TypeRegistry.get.requiredType"
			);
		});

		it("should reject if an invalid type name is passed", () => {
			const args = [{}];
			const expected = ERRORS.TypeRegistry.get.requiredType(...args);
			assert.rejects(
				() => Instance.get(...args), expected,
				"should throw ERRORS.TypeRegistry.get.requiredType"
			);
		});

		it("should reject if an invalid member name is passed", () => {
			const args = ["a", {}];
			const expected = ERRORS.TypeRegistry.get.requiredMember(...args);
			assert.rejects(
				() => Instance.get(...args), expected,
				"should throw ERRORS.TypeRegistry.get.requiredType"
			);
		});

		it("should return a new promise if the type does not yet exist", () => {
			const Private = spyWeakMap.get(Instance);

			const fakeTypeName = randomString();

			assert.ok(
				!Private.registry.has(fakeTypeName),
				"type should not exist in registry yet"
			);
			assert.ok(
				!Private.resolvers.has(fakeTypeName),
				"type should not have a resolver yet"
			);
			assert.ok(
				!Private.promises.has(fakeTypeName),
				"type should not have a promise yet"
			);

			const actual = Instance.get(fakeTypeName);
			const expected = Private.promises.get(fakeTypeName);

			// Registry signifies when a type has been registered
			assert.ok(
				!Private.registry.has(fakeTypeName),
				"type still shouldn't exist in registry yet"
			);

			// Resolvers and promises should be populated regardless
			assert.ok(
				Private.resolvers.has(fakeTypeName),
				"type should have a resolver now"
			);
			assert.ok(
				Private.promises.has(fakeTypeName),
				"type should have a promise now"
			);
			assert.strictEqual(actual, expected);
		});

		it("should return the same promise for every request to the same type", () => {
			const Private = spyWeakMap.get(Instance);

			const fakeTypeName = randomString();

			const actual1 = Instance.get(fakeTypeName);
			const actual2 = Instance.get(fakeTypeName);
			const expected = Private.promises.get(fakeTypeName);

			assert.strictEqual(actual1, expected);
			assert.strictEqual(actual2, expected);
		});

		it("should reject if invalid type name is passed with member name", () => {
			assert.rejects(() => Instance.get({}, "a"), "must be");
		});

		it("should reject if non-empty invalid member name is passed", () => {
			assert.rejects(() => Instance.get("a", {}), "must be");
			assert.rejects(() => Instance.get("a", ""), "must be");
		});

		it("should call get on Member Registry if member is specified", () => {
			const Private = spyWeakMap.get(Instance);

			const fakeTypeName = randomString();
			const fakeMemberName = randomString();

			const MemberRegInstance = new MemberRegistry({
				type: fakeTypeName,
				promise: Promise.resolve(mockParent)
			}, mockExorsa);

			MemberRegInstance.register({
				name: fakeMemberName,
				member: {}
			});

			const actual = Instance.get(fakeTypeName, fakeMemberName);
			const expected = Private.members
				.get(fakeTypeName)
				.get(fakeMemberName);

			assert.strictEqual(actual, expected);
		});
	});

	describe("define()", () => {
		it("should reject if an invalid TypeClass is passed", () => {
			let expected;
			const itShould = "should throw ERRORS.TypeRegistry.define.typeIsNotClass";

			let args = [];
			expected = ERRORS.TypeRegistry.define.typeIsNotClass(...args);
			assert.rejects(Instance.define(...args), expected, itShould);

			args = ["a"];
			expected = ERRORS.TypeRegistry.define.typeIsNotClass(...args);
			assert.rejects(Instance.define(...args), expected, itShould);

			args = ["a", "b"];
			expected = ERRORS.TypeRegistry.define.typeIsNotClass(...args);
			assert.rejects(Instance.define(...args), expected, itShould);
		});

		it("should reject if the type has already been registered", () => {
			const fakeTypeName = randomString();
			const { registry } = spyWeakMap.get(Instance);

			registry.add(fakeTypeName);

			const expected = (
				ERRORS.TypeRegistry.define.typeAlreadyDefined(fakeTypeName)
			);
			const itShould = "should throw ERRORS.TypeRegistry.define.typeAlreadyDefined";

			assert.rejects(Instance.define(
				fakeTypeName,
				class {}
			), expected, itShould);
		});

		it("should register and initialize a valid type", () => {
			const Private = spyWeakMap.get(Instance);
			const mock = mockMemberDescriptor;

			// Make sure the type hasn't been registered yet
			assert.ok(
				!Private.registry.has(mock.name),
				"type should not exist in registry yet"
			);

			// We also want to make sure the promises and resolvers are being created
			assert.ok(
				!Private.resolvers.has(mock.name),
				"type should not have a resolver yet"
			);
			assert.ok(
				!Private.promises.has(mock.name),
				"type should not have a promise yet"
			);

			const promiseActual = Instance.define(mock.name, mock);

			return promiseActual.then((type) => {
				const promiseExpected = Private.promises.get(mock.name);
				assert.strictEqual(promiseActual, promiseExpected);

				assert.ok(
					Private.registry.has(mock.name),
					"should add the type's name to the registry"
				);

				assert.ok(
					type instanceof mockMemberDescriptor,
					"should resolve the stored promise"
				);
			});
		});

		it("should register and initialize a valid type without optional name parameter", () => {
			const Private = spyWeakMap.get(Instance);
			const mock = mockMemberDescriptor;

			// Make sure the type hasn't been registered yet
			assert.ok(
				!Private.registry.has(mock.name),
				"type should not exist in registry yet"
			);

			// We also want to make sure the promises and resolvers are being created
			assert.ok(
				!Private.resolvers.has(mock.name),
				"type should not have a resolver yet"
			);
			assert.ok(
				!Private.promises.has(mock.name),
				"type should not have a promise yet"
			);

			const promiseActual = Instance.define(mock);

			return promiseActual.then((type) => {
				const promiseExpected = Private.promises.get(mock.name);
				assert.strictEqual(promiseActual, promiseExpected);

				assert.ok(
					Private.registry.has(mock.name),
					"should add the type's name to the registry"
				);

				assert.ok(
					type instanceof mockMemberDescriptor,
					"should resolve the stored promise"
				);
			});
		});

		it("should reject if instantiation of TypeClass fails", () => {
			const Private = spyWeakMap.get(Instance);

			const expectedError = "should fail";

			const mock = class {
				static name = "typename";

				static type = fakeType;

				constructor() {
					throw new Error(expectedError);
				}
			};

			// Make sure the type hasn't been registered
			assert.ok(
				!Private.registry.has(mock.name),
				"type should not exist in registry"
			);

			// We also want to make sure the promises and resolvers are being created
			assert.ok(
				!Private.resolvers.has(mock.name),
				"type should not have a resolver"
			);
			assert.ok(
				!Private.promises.has(mock.name),
				"type should not have a promise"
			);

			assert.rejects(Instance.define(mock.name, mock), expectedError);
		});
	});
});

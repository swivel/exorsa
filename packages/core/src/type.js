import { ERRORS } from "./copy.js";

/**
 * Private: WeakMap for storing instance properties that shouldn't
 * be accessed publicly. Automatically garbage collected when instance
 * is deconstructed and references are purged.
 *
 * @type {WeakMap}
 */
const Private = new WeakMap();

export const genBindK = (agent) => (k) => agent[k].bind(agent);
export const genGetK = (agent) => (k) => agent[k];

/**
 * ExorsaType
 */
export default class ExorsaType {
	constructor({ type, registry }, agent) {
		Private.set(this, {
			agent,
			type,
			registry
		});
	}

	get framework() {
		const { agent } = Private.get(this);
		return agent;
	}

	get type() {
		const { type } = Private.get(this);
		return type;
	}

	get registry() {
		const { registry } = Private.get(this);
		return registry;
	}

	get interface() {
		const { agent } = Private.get(this);

		const bindK = genBindK(agent);
		const getK = genGetK(agent);

		return Object.freeze(
			Object.keys(agent).reduce((o, k) => {
				let getter = getK;
				if (typeof agent[k] === "function") {
					getter = bindK;
				}

				return ({
					...o,
					get [k]() { return getter(k); }
				});
			}, {})
		);
	}

	initialize(/* {
		name,
		type,
		member,
		dependencies,
		originalDescriptor
	} */) {
		throw new Error(
			ERRORS.ExorsaType.initialize(this.constructor.name)
		);
	}
}

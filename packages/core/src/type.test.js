/* eslint-disable import/first, no-unused-vars */

import assert from "assert";
import sinon from "sinon";
import crypto from "crypto";
import { WeakMapSpy, randomString } from "../../../test-utils.js";
import { ERRORS } from "./copy.js";

import ExorsaType from "./type.js";
/* eslint-enable no-unused-vars */

let spyWeakMap;
let mockRegistry;
let mockExorsa;
let mockTypeName;
let Instance;

describe("@exorsa/core::ExorsaType", () => {
	before(() => {
		spyWeakMap = new WeakMapSpy();

		mockTypeName = "MyType";

		mockRegistry = new Set();
		mockExorsa = {
			test1: randomString(),
			test2: sinon.fake.returns(function bindable() { return this; }),
		};
	});

	beforeEach(() => {
		Instance = new ExorsaType({
			type: mockTypeName,
			registry: mockRegistry,
		}, mockExorsa);
	});

	afterEach(() => {
		spyWeakMap.delete(Instance);
	});

	describe("constructor()", () => {
		it("should store arguments in a private weakmap", () => {
			const Private = spyWeakMap.get(Instance);

			assert.strictEqual(Private.agent, mockExorsa);
			assert.strictEqual(Private.type, mockTypeName);
			assert.strictEqual(Private.registry, mockRegistry);
			assert.ok(
				Private.registry instanceof Set,
				"registry should be an instance of a Set"
			);
		});
	});

	describe("get exorsa()", () => {
		it("should return the exorsa property within the Private weakmap", () => {
			const Private = spyWeakMap.get(Instance);

			assert.strictEqual(Private.agent, mockExorsa);
			assert.strictEqual(Instance.framework, mockExorsa);
		});
	});

	describe("get type()", () => {
		it("should return the type property within the Private weakmap", () => {
			const Private = spyWeakMap.get(Instance);

			assert.strictEqual(Private.type, mockTypeName);
			assert.strictEqual(Instance.type, mockTypeName);
		});
	});

	describe("get registry()", () => {
		it("should return the registry property within the Private weakmap", () => {
			const Private = spyWeakMap.get(Instance);

			assert.strictEqual(Private.registry, mockRegistry);
			assert.strictEqual(Instance.registry, mockRegistry);
		});
	});

	describe("get interface()", () => {
		it("should return a frozen interface to agent", () => {
			const { agent } = spyWeakMap.get(Instance);

			const Interface = Instance.interface;

			const agentKeys = Object.keys(agent);
			const interfaceKeys = Object.keys(Interface);
			const allUKeys = new Set([
				...agentKeys,
				...interfaceKeys
			]);

			// Must be frozen
			assert.ok(
				Object.isFrozen(Interface),
				"interface should be frozen"
			);

			/* eslint-disable no-restricted-syntax, guard-for-in */
			for (const k in allUKeys) {
				assert.ok(k in agent, "key should be in agent");
				assert.ok(k in Interface, "key should be in Interface");

				assert.ok(
					typeof agent[k] !== "function"
					|| Interface[k]() === agent,
					"Interface should not return the agent"
				);

				assert.strictEqual(agent[k], Interface[k]);
			}
			/* eslint-enable no-restricted-syntax, guard-for-in */
		});
	});

	describe("initialize()", () => {
		it("should not be implemented on the base class", () => {
			const expected = ERRORS.ExorsaType.initialize(
				Instance.constructor.name
			);
			assert.throws(
				Instance.initialize.bind(Instance), (err) => {
					assert.equal(err.message, expected);
					return true;
				}, "should throw ERRORS.ExorsaType.initialize"
			);
		});
	});
});

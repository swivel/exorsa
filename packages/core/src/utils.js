import { ERRORS } from "./copy.js";

/**
 * Determines if an object is a "class", or an object with a prototype.
 *
 * @method isClass
 * @param  {any} obj Object to be tested
 * @return {Boolean} True if object is considered a prototype.
 */
export const isClass = (test) => (
	typeof test === "function"
	&& "prototype" in test
	&& "constructor" in test.prototype
	&& test.prototype.constructor === test
);

/**
 * Formats a string in a uniform format
 *
 * @method formatName
 * @param  {string} str String to be formatted
 * @return {String} Formatted String
 */
export const formatName = (str) => `${str}`.toLowerCase();

/**
 * Checks a map for an existing key
 * If it doesn't exist, it sets it to the return of the value callback function
 *
 * @method initializeIfNeeded
 * @param  {Map} map Map to be searched
 * @param  {String} name Key to use for search
 * @param  {function} [value] Callback used to generate value if key is missing
 * @return {any} Value of name key in map
 */
export const initializeIfNeeded = (map, name, value = () => new Map()) => {
	if (!map.has(name)) map.set(name, value());
	return map.get(name);
};

/**
 * Retrieves the key requested, optionally creating it if it does not exist
 *
 * e.g.,
 * deepGet(obj, ['bar', 'baz'], true);
 * returns obj.bar.baz || {};
 *
 * @param {object}  obj    The object to search
 * @param {array}   keys   Array of keys to traverse
 * @param {boolean} create If true, populates the key if empty
 * @return {any} Returns the value of the keys specified
 */
export const deepGet = (obj, keys, create = false) => {
	const keyChain = [].concat(keys);

	return keyChain.reduce((o, key) => {
		if (typeof o !== "object") {
			throw new TypeError(
				ERRORS.utils.deepGet.getFromNonObject(
					keyChain.join("."), key, typeof o
				)
			);
		} else if (key in o) {
			return o[key];
		}

		if (create === false) {
			throw new TypeError(
				ERRORS.utils.deepGet.getFromNonObject(
					keyChain.join("."), key, typeof o
				)
			);
		}

		/* eslint-disable no-param-reassign */
		o[key] = {};
		/* eslint-enable */

		return o[key];
	}, obj);
};

/**
 * Sets the key specified to the value
 *
 * e.g.,
 * deepSet(obj, ['bar','baz'], false);
 * Sets: obj.bar.baz = false;
 *
 * @param {object} obj    Object to modify
 * @param {array}  keys   Array of keys
 * @param {any}    values The value to set
 * @return {object} The resulting object
 */
export const deepSet = (obj, keys, value) => {
	const keyChain = [].concat(keys);
	const key = keyChain.pop();
	const parent = deepGet(obj, keyChain, true);
	parent[key] = value;
	return obj;
};

/**
 * Converts a complex array of keys and an array of values into an object.
 *
 * e.g.,
 * The following: deepArrayToObject([['foo'],['bar','baz']], [true,false])
 * Becomes: { foo: true, bar: { baz: false } }
 *
 * @method deepArrayToObject
 * @param {array} keys   Multidimensional array of keys
 * @param {array} values Array of values
 * @return {object} Merged objected
 */
export const deepArrayToObject = (keys, values) => keys.reduce(
	(obj, key, i) => {
		if (Array.isArray(key)) {
			return deepSet(obj, key, values[i]);
		}
		return { ...obj, [key]: values[i] };
	}, {}
);

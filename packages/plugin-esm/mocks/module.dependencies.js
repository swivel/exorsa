import sinon from "sinon";

export const dependencies = [
  ['some_type', 'some_member']
];

export const module = sinon.fake();

export default module;

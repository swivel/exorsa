export const genPathBuilder = (modulePath) => {
	let getPath;
	if (typeof modulePath === "string") {
		getPath = (type, member, ext = "") => `${modulePath}/${type}${member ? `/${member}` : ""}${ext}`;
	} else if (typeof modulePath === "function") {
		getPath = modulePath;
	} else {
		throw new TypeError(`exorsa.use( ExorsaESMPlugin( modulePath ) ): modulePath must be a string or function, not ${typeof modulePath}`);
	}

	return getPath;
};

export const handleTypeImported = (type, member, fx, {
	define = false,
	...esmExports
}) => {
	const module = esmExports.default || esmExports.module;

	if (!module) {
		throw new TypeError(`exorsa.plugins.esm: Failed to import Type. Missing default or "module" named export: ${type}`);
	}

	if (define !== false) {
		define({
			type,
			module,
			exports: {
				...esmExports
			}
		}, fx);

		return;
	}

	fx.define(type, module);
};

export const handleMemberImported = (type, member, fx, {
	dependencies = [],
	register = false,
	...esmExports
}) => {
	const module = esmExports.default || esmExports.module;

	if (!module) {
		throw new TypeError(`exorsa.plugins.esm: Failed to import Type Member. Missing default or "module" named export: ${type}:${member}`);
	}

	if (register !== false) {
		register({
			type,
			member,
			module,
			exports: {
				dependencies,
				...esmExports,
			}
		}, fx);

		return;
	}

	fx.register(type, {
		name: member,
		member: module,
		dependencies
	});
};

const ExorsaESMPlugin = (modulePath, ext = ".js") => {
	const getPath = genPathBuilder(modulePath);

	return (type, memberName, fx = memberName) => {
		let member;
		if (memberName !== fx) member = memberName;

		return import(getPath(type, member, ext, fx))
			.then((importedModule) => {
				if (!member) {
					handleTypeImported(type, member, fx, importedModule);
				} else {
					handleMemberImported(type, member, fx, importedModule);
				}
			});
	};
};

export default ExorsaESMPlugin;

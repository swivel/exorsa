import sinon from "sinon";
import assert from "assert";

import ExorsaESMPlugin, { genPathBuilder } from "./index.js";

import * as mockModuleBare from "../mocks/module.bare.js";
import * as mockModuleNonDefExp from "../mocks/module.non-default-exp.js";
import * as mockModuleDeps from "../mocks/module.dependencies.js";
import * as mockModuleRegister from "../mocks/module.custom-register.js";

import * as mockTypeBare from "../mocks/type.bare.js";
import * as mockTypeNonDefExp from "../mocks/type.non-default-exp.js";
import * as mockTypeDefine from "../mocks/type.custom-define.js";

let mockExorsa;

describe("@exorsa/plugins-esm", () => {
	beforeEach(() => {
		mockExorsa = {
			define: sinon.fake(),
			register: sinon.fake()
		};
	});

	describe("import(type)", () => {
		it("should import and define a Type with a solitary default export", () => {
			const type = "type.bare";
			const ext = ".js";
			const root = "../mocks";

			const plugin = ExorsaESMPlugin(root, ext);

			const ret = plugin(type, mockExorsa);

			return ret.then(() => {
				assert.deepStrictEqual(
					mockExorsa.define.lastCall.firstArg, type
				);
				assert.deepStrictEqual(
					mockExorsa.define.lastCall.lastArg, mockTypeBare.default
				);

				// Make sure the module wasn't called by the loader
				assert.strictEqual(mockTypeBare.default.lastCall, null);
			});
		});

		it("should import and register a Type Member with a solitary module export", () => {
			const type = "type.non-default-exp";
			const ext = ".js";
			const root = "../mocks";

			const plugin = ExorsaESMPlugin(root, ext);

			const ret = plugin(type, mockExorsa);

			return ret.then(() => {
				assert.deepStrictEqual(
					mockExorsa.define.lastCall.firstArg, type
				);
				assert.deepStrictEqual(
					mockExorsa.define.lastCall.lastArg, mockTypeNonDefExp.module
				);

				// Make sure the module wasn't called by the loader
				assert.strictEqual(mockTypeNonDefExp.module.lastCall, null);
			});
		});

		it("should import a Type Member and execute its own define function", () => {
			const type = "type.custom-define";
			const ext = ".js";
			const root = "../mocks";

			const plugin = ExorsaESMPlugin(root, ext);

			const { define, ...esmExports } = mockTypeDefine;

			const expected = {
				type,
				module: mockTypeDefine.default,
				exports: {
					...esmExports,
				}
			};

			const expectedCallCount = mockExorsa.define.callCount;
			const ret = plugin(type, mockExorsa);

			return ret.then(() => {
				// Make sure define wasn't called
				assert.strictEqual(
					expectedCallCount, mockExorsa.define.callCount
				);

				assert.deepStrictEqual(define.lastCall.firstArg, expected);
				assert.deepStrictEqual(define.lastCall.lastArg, mockExorsa);

				// Make sure the module wasn't called by the loader
				assert.strictEqual(mockTypeDefine.default.lastCall, null);
			});
		});

		it("should reject if Type Member has no default or named 'module' export", () => {
			const type = "type.no-export";
			const ext = ".js";
			const root = "../mocks";

			const plugin = ExorsaESMPlugin(root, ext);

			assert.rejects(plugin(type, mockExorsa), /[Ff]ailed to import/);
		});
	});

	describe("import(type, module)", () => {
		it("should import and register a Type with a solitary default export", () => {
			const member = "module.bare";
			const ext = ".js";
			const [root, type] = ["../", "mocks"];

			const plugin = ExorsaESMPlugin(root, ext);

			const expected = {
				name: member,
				member: mockModuleBare.default,
				dependencies: []
			};

			const ret = plugin(type, member, mockExorsa);

			return ret.then(() => {
				assert.deepStrictEqual(
					mockExorsa.register.lastCall.firstArg, type
				);
				assert.deepStrictEqual(
					mockExorsa.register.lastCall.lastArg, expected
				);

				// Make sure the module wasn't called by the loader
				assert.strictEqual(mockModuleBare.default.lastCall, null);
			});
		});

		it("should import and register a Type with a solitary module export", () => {
			const member = "module.non-default-exp";
			const ext = ".js";
			const [root, type] = ["../", "mocks"];

			const plugin = ExorsaESMPlugin(root, ext);

			const expected = {
				name: member,
				member: mockModuleNonDefExp.module,
				dependencies: []
			};

			const ret = plugin(type, member, mockExorsa);

			return ret.then(() => {
				assert.deepStrictEqual(
					mockExorsa.register.lastCall.firstArg, type
				);
				assert.deepStrictEqual(
					mockExorsa.register.lastCall.lastArg, expected
				);

				// Make sure the module wasn't called by the loader
				assert.strictEqual(mockModuleNonDefExp.module.lastCall, null);
			});
		});

		it("should import and register a Type with dependencies", () => {
			const member = "module.dependencies";
			const ext = ".js";
			const [root, type] = ["../", "mocks"];

			const plugin = ExorsaESMPlugin(root, ext);

			const expected = {
				name: member,
				member: mockModuleDeps.default,
				dependencies: mockModuleDeps.dependencies
			};

			const ret = plugin(type, member, mockExorsa);

			return ret.then(() => {
				assert.deepStrictEqual(
					mockExorsa.register.lastCall.firstArg, type
				);
				assert.deepStrictEqual(
					mockExorsa.register.lastCall.lastArg, expected
				);

				// Make sure the module wasn't called by the loader
				assert.strictEqual(mockModuleDeps.default.lastCall, null);
			});
		});

		it("should import a Type and execute its own register function", () => {
			const member = "module.custom-register";
			const ext = ".js";
			const [root, type] = ["../", "mocks"];

			const plugin = ExorsaESMPlugin(root, ext);

			const { register, ...esmExports } = mockModuleRegister;

			const expected = {
				type,
				member,
				module: mockModuleRegister.default,
				exports: {
					...esmExports,
				}
			};

			const expectedCallCount = mockExorsa.register.callCount;
			const ret = plugin(type, member, mockExorsa);

			return ret.then(() => {
				// Make sure register wasn't called
				assert.strictEqual(
					expectedCallCount, mockExorsa.register.callCount
				);

				assert.deepStrictEqual(register.lastCall.firstArg, expected);
				assert.deepStrictEqual(register.lastCall.lastArg, mockExorsa);

				// Make sure the module wasn't called by the loader
				assert.strictEqual(mockModuleRegister.default.lastCall, null);
			});
		});

		it("should reject if Type has no default or named 'module' export", () => {
			const member = "module.no-export";
			const ext = ".js";
			const [root, type] = ["../", "mocks"];

			const plugin = ExorsaESMPlugin(root, ext);

			assert.rejects(
				plugin(type, member, mockExorsa),
				/[Ff]ailed to import/
			);
		});
	});

	describe("genPathBuilder()", () => {
		it("should return a function that concatenates the modulePath, type, member, and ext by directory separates", () => {
			const parts = ["foo", "bar", "baz", ".js"];
			const getPath = genPathBuilder(parts[0]);
			const actual = getPath(...parts.slice(1));

			const expected = `${parts.slice(0, -1).join("/")}${parts.slice(-1)}`;

			assert.strictEqual(actual, expected);
		});

		it("should return a function that concatenates the modulePath, type, and ext by directory separates if no member is passed", () => {
			const parts = ["foo", "bar", void 0, ".js"];
			const getPath = genPathBuilder(parts[0]);
			const actual = getPath(...parts.slice(1));

			const expected = `${parts.slice(0, -2).join("/")}${parts.slice(-1)}`;

			assert.strictEqual(actual, expected);
		});

		it("should return the modulePath if it is a function", () => {
			const modulePath = sinon.fake();
			const getPath = genPathBuilder(modulePath);

			assert.strictEqual(getPath, modulePath);
		});

		it("should throw an error if modulePath is not a string or a function", () => {
			assert.throws(() => genPathBuilder());
			assert.throws(() => genPathBuilder(true));
			assert.throws(() => genPathBuilder(12345));
			assert.throws(() => genPathBuilder(123.5));
			assert.throws(() => genPathBuilder({}));
			assert.throws(() => genPathBuilder([]));
			assert.throws(() => genPathBuilder(new Set()));
			assert.throws(() => genPathBuilder(new WeakMap()));
		});
	});
});
